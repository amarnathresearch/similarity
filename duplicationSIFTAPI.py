#!/usr/bin/env python
# coding: utf-8

# In[31]:


import numpy as np
import cv2
import math
import os
import sys
# In[32]:


def initializeSIFT():
    orb = cv2.ORB()
    orb = cv2.ORB_create()
    return orb

def getDistance(orb, img1, img2):
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)
    if len(kp2) == 0 or len(kp1) == 0:
        return 1000
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1,des2)
    matches = sorted(matches, key = lambda x:x.distance)
    firstTen = matches[:10]
    s = 0;
    for i in firstTen:
        s = s + i.distance
    return s/10

def main(path, T):
    n = 0
    for entry in os.scandir(path):
        if entry.is_file():
            n = n + 1
    # print(n)

    s = (n, n)
    names = ["" for x in range(n)]
    distances = np.zeros(s)
    # print(names)

    i = 0
    for entry in os.scandir(path):
        if entry.is_file():
            names[i] = entry.name
            i = i + 1

    orb = initializeSIFT()
    folder = path
    for i in range(n-1):
        for j in range(i+1, n):
            img1 = cv2.imread(folder+''+names[i],0)
            img2 = cv2.imread(folder+''+names[j],0)

            dis = getDistance(orb, img1, img2)
            distances[i, j] = dis
    
    jsonKey = {};
    for i in range(n):
        jsonList = {}
        jsonList['isDuplication'] = 'No'
        jsonList['duplicationList'] = {}
        jsonKey[names[i]] = jsonList

    for i in range(n-1):
        jsonScore = {}
        for j in range(i+1, n):
            if(distances[i, j] < T):
                jsonScore[names[i]] = distances[i, j]
                jsonKey[names[j]]['isDuplication'] = 'Yes'
                jsonKey[names[j]]['duplicationList'] = jsonScore
                
    return jsonKey
                


##########################################################
# Call main function 
# Input: Two arguments:
#        path = 'images/'
#        T = 20
# Output: Json

if __name__ == "__main__":
    path = sys.argv[1]
    T = int(sys.argv[2])
#    T = 20
#    path = 'images/'
    jsonKey = main(path, T)
    print(jsonKey)
    sys.stdout.flush()


############################################################333




